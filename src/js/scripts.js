$(function () {
    $("posts").on("click", ".read-more", function () {
        $(this).parent().find("span.hidden.ctn").togggleClass("d-none")
    });

    let images = $(".carousel-item").map((i, item) =>
        $(item.children[0]).attr("src")
    );
    let randomeImageIndex = Math.round(Math.random() * (images.length - 1));


    $(".carousel-item.active").find("img").attr("src", images[randomeImageIndex]);

    $(".carousel-inner")
        .find(`.carousel-item:nth-child(${randomeImageIndex})`)
        .find("img")
        .attr("src", images[0]);
});